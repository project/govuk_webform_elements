GOV.UK Design System Webform elements 8.x.1.0

INTRODUCTION
------------

GOV.UK Design System Webform elements builds upon the Webform module
to provide GOV.UK Design System Webform elements.

Currently this project consists of a GOV.UK Design System
composite date element (more coming).


REQUIREMENTS
------------

This module requires the following modules:

Webform (https://www.drupal.org/project/webform)


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit:
https://drupal.org/documentation/install/modules-themes/modules-8
for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. Configuration is performed
via the Webform UI


MAINTAINERS
-----------

John Burch (webfaqtory) https://www.drupal.org/u/webfaqtory

This project has been sponsored by:

ACAS (www.acas.org.uk) The Advisory, Conciliation and Arbitration Service
is a Crown non-departmental public body of the Government of the United Kingdom.
